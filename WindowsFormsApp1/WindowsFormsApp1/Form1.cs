﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public static string userInfo;
        
        public static string jsonString;
        Excel.Application xlApp;

        public Form1()
        {
            InitializeComponent();
            flowLayoutPanel1.BringToFront();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://assed-e31ad.firebaseio.com/.json");
            //request.Method = "POST";
            request.ContentType = "application/json";
            HttpWebResponse response2 = request.GetResponse() as HttpWebResponse;
            using (Stream responseStream = response2.GetResponseStream())
            {
                StreamReader read = new StreamReader(responseStream, Encoding.UTF8);
                jsonString = read.ReadLine();


            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            flowLayoutPanel1.BringToFront();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            flowLayoutPanel2.BringToFront();

        }

        private void docPrefab1_Load(object sender, EventArgs e)
        {
            
        }

      

        private void panel6_Paint(object sender, PaintEventArgs e)
        {
           
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            JObject jobject = JObject.Parse(jsonString);
            JObject user = (JObject)jobject["JrPwbApfIHbQhCUmVIoiVJcPYv93"];
            textBox1.Text = user.ToString();
            userInfo = user.ToString();
            Form2 form2 = new Form2();
            //form2.userInfoFromJson = textBox1.Text;
            form2.Show();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }
    }
}

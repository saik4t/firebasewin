﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Doctor_Details : Form
    {
        PictureBox[] picbox;
        Image[] img;
        public string local_doc_json = docPrefab.jsonDoc;
        string[] adressURlArray;
        List<string> adressURLList = new List<string>();
        public Doctor_Details()
        {
            InitializeComponent();
            
            var result = ParseJson(local_doc_json);
            foreach (var r in result)
            {
                //Console.WriteLine("Key={0};Name={1};Address={2}", r.Key, r.Value.name, r.Value.address);
                adressURLList.Add(r.Value.ToString());
            }
            adressURlArray = adressURLList.ToArray();
            textBox1.Text = adressURlArray[0];

            //var request = WebRequest.Create(adressURlArray[0]+".jpg");
            //pictureBox1.ImageLocation = adressURlArray[0];
            //using (var response = request.GetResponse())
            //using (var stream = response.GetResponseStream())
            //{
             //   pictureBox1.Image = Bitmap.FromStream(stream);
            //}
        }

        private void Doctor_Details_Load(object sender, EventArgs e)
        {
            string sURL = adressURlArray[0];

            pictureBox1.ImageLocation = sURL;
            for (int i = 0; i < adressURlArray.Length; i++)
            {
                //WebRequest req = WebRequest.Create(adressURlArray[i]);

                //WebResponse res = req.GetResponse();

                //Stream imgStream = res.GetResponseStream();



                //img[i] = Image.FromStream(imgStream);

                //imgStream.Close();


               PictureBox pb = new PictureBox();
                //picbox[i].Name = "pic" + i;
                pb.Size = pictureBox1.Size;
                pb.SizeMode = PictureBoxSizeMode.Zoom;
               pb.ImageLocation = adressURlArray[i];
                flowLayoutPanel1.Controls.Add(pb);
            }
        }
        
        public static Dictionary<string, string> ParseJson(string source)
        {
            return JsonConvert.DeserializeObject<Dictionary<string, string>>(source);
        }
    }
}

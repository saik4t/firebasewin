﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public string userInfoFromJson = Form1.userInfo;
        List<string> nameList = new List<string>();
        List<string> adressList = new List<string>();
        List<string> miscList = new List<string>();
        List<string> productList = new List<string>();
        List<string> timeList = new List<string>();
        string[] nameArray;
        string[] adressArray;
        string[] timeArray;
        string[] miscArray;
        string[] productArray;
        Excel.Application xlApp;
        public Form2()
        {
            
            InitializeComponent();
            
            textBox1.Text = userInfoFromJson;
            var result = ParseJson(userInfoFromJson);

            foreach (var r in result)
            {
                //Console.WriteLine("Key={0};Name={1};Address={2}", r.Key, r.Value.name, r.Value.address);
                nameList.Add(r.Value.name);
                adressList.Add(r.Value.address);
                miscList.Add(r.Value.Misc);
                productList.Add(r.Value.Product);
                timeList.Add(r.Key);
            }
            nameArray = nameList.ToArray();
            adressArray = adressList.ToArray();
            timeArray = timeList.ToArray();
            miscArray = miscList.ToArray();
            productArray = productList.ToArray();

            //dataGridView2.Rows[0].Cells[0].Value = nameArray[0];
            //dataGridView2.Rows[1].Cells[0].Value = nameArray[1];
            for (int i = 0; i < nameArray.Length; i++)
            {
                dataGridView2.Rows.Add();
                dataGridView2.Rows[i].Cells[0].Value = nameArray[i];
                dataGridView2.Rows[i].Cells[1].Value = adressArray[i];
                dataGridView2.Rows[i].Cells[2].Value = timeArray[i];
                dataGridView2.Rows[i].Cells[3].Value = productArray[i];
                dataGridView2.Rows[i].Cells[4].Value = miscArray[i];
            }
            label1.Text = "working on it";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            xlApp = new Microsoft.Office.Interop.Excel.Application();

            if (xlApp == null)
            {
                label1.Text = "Excel is not properly installed!!";
                return;
            }
            saveFileDialog1.ShowDialog();
        }
        private void saveFileDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            
        }
        public class Item
        {

            public string name;
            public string address;
            public string Product;
            public string Misc;
            //public string time;

        }
        public static Dictionary<string, Item> ParseJson(string source)
        {
            return JsonConvert.DeserializeObject<Dictionary<string, Item>>(source);
        }

        private void saveFileDialog1_FileOk_1(object sender, CancelEventArgs e)
        {
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            //xlWorkSheet.Cells[1, 1] = "ID";
            //xlWorkSheet.Cells[1, 2] = "Name";
            //xlWorkSheet.Cells[2, 1] = "1";
            //xlWorkSheet.Cells[2, 2] = "One";
            //xlWorkSheet.Cells[3, 1] = "2";
            //xlWorkSheet.Cells[3, 2] = "Two";

            for (int i = 1; i <= nameArray.Length; i++)
            {
                xlWorkSheet.Cells[i, 1] = nameArray[i - 1];
            }
            for (int i = 1; i <= adressArray.Length; i++)
            {
                xlWorkSheet.Cells[i, 2] = adressArray[i - 1];
            }
            for (int i = 1; i <= timeArray.Length; i++)
            {
                xlWorkSheet.Cells[i, 3] = timeArray[i - 1];
            }
            for (int i = 1; i <= productArray.Length; i++)
            {
                xlWorkSheet.Cells[i, 4] = productArray[i - 1];
            }
            for (int i = 1; i <= miscArray.Length; i++)
            {
                xlWorkSheet.Cells[i, 5] = miscArray[i - 1];
            }




            xlWorkBook.SaveAs(saveFileDialog1.FileName.ToString(), Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);

            label1.Text = "Excel file created , you can find the file" + saveFileDialog1.FileName.ToString();
        }
    }
}
